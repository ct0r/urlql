const test = require('ava')
const { resolve } = require('.')

test('test', t => {
  const query = resolve('name=like~TV%3A&price=min~50;max~100&resolution=in~4K,1080p', {
    name: (args, query) => ({ ...query, name: args }),
    price: (args, query) => ({ ...query, price: args }),
    resolution: (args, query) => ({ ...query, resolution: args })
  }, { init: true })

  t.deepEqual(query, {
    init: true,
    name: { like: 'TV:' },
    price: { min: '50', max: '100' },
    resolution: { in: ['4K', '1080p'] }
  })
})
