const { parse, unescape } = require('querystring')

function resolve (qs, schema, initialValue = {}) {
  const params = parse(qs, null, null, { decodeURIComponent: str => str })

  return Object.entries(params)
    .map(([key, value]) => ([
      key,
      value.split(';')
        .reduce((args, pair) => {
          const [key, value] = pair.split('~')

          const values = value.split(',')
            .map(unescape)

          const fnInput = values.length > 1
            ? values
            : values[0]

          return { ...args, [key]: fnInput }
        }, {})
    ]))
    .reduce((query, [key, value]) => schema[key](value, query), initialValue)
}

module.exports = { resolve }
