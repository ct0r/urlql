# urlql

When standard query sring is not good enough and graphql is an overkill.

## Motivation


## Install

```
npm install gitlab:ct0r/urlql#semver:[version]
```

## Usage

### Building mongo query

```js
const { resolve } = require("urlql");

const query = resolve(
  "name=like~TV&price=gt~250;lt~300&resolution=in~4K,1080p",
  {
    name: ({ like }, qry) => ({ ...qry, name: { $regex: like } }),
    price: ({ gt, lt }, qry) => ({ ...qry, price: { $gt: gt, $lt: lt } })
  },
  { visible: true }
);
```
